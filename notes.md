# Spring Boot 2 with React and Redux

### Things Learned / Troubles
* Validation errors were being swallowed for invalid Tasks but not invalid Projects. The TaskController method body was not being executed since the error was being intercepted
    * This was resolved by **making sure** the the BindingResult comes IMMEDIATELY after the parameter marked as @Valid

### Section 2 Lecture 10

- You can format the json output of entity fields by using the @JsonFormat annotation
- The following will output "2019-02-16" instead of "2019-02-16T12:41:24.854" on the response

```java
    @JsonFormat(pattern = "yyyy-mm-dd")
    private LocalDate startDate;
```

### Section 2 Lecture 11

- Validation constraints will return a 500 error by default
- If you annotate the requrestbody with @Valid, then the response will be a bad request and the formatting of the error messages will be far superior

```json
{
    "timestamp": "2019-02-16T17:49:37.529+0000",
    "status": 500,
    "error": "Internal Server Error",
    "message": "Validation failed for classes [com.zagsoft.ppmtool.project.Project] during persist time for groups [javax.validation.groups.Default, ]\nList of constraint violations:[\n\tConstraintViolationImpl{interpolatedMessage='Project name is required', propertyPath=projectName, rootBeanClass=class com.zagsoft.ppmtool.project.Project, messageTemplate='Project name is required'}\n\tConstraintViolationImpl{interpolatedMessage='Project description is required', propertyPath=description, rootBeanClass=class com.zagsoft.ppmtool.project.Project, messageTemplate='Project description is required'}\n\tConstraintViolationImpl{interpolatedMessage='Project identifier is required', propertyPath=projectIdentifier, rootBeanClass=class com.zagsoft.ppmtool.project.Project, messageTemplate='Project identifier is required'}\n]",
    // ...
}
// Versus:

{
    "timestamp": "2019-02-16T17:46:13.234+0000",
    "status": 400,
    "error": "Bad Request",
    "errors": [
        {
            "codes": [
                "NotBlank.project.projectIdentifier",
                "NotBlank.projectIdentifier",
                "NotBlank.java.lang.String",
                "NotBlank"
            ],
            "arguments": [
                {
                    "codes": [
                        "project.projectIdentifier",
                        "projectIdentifier"
                    ],
                    "arguments": null,
                    "defaultMessage": "projectIdentifier",
                    "code": "projectIdentifier"
                }
            ],
            "defaultMessage": "Project identifier is required",
            "objectName": "project",
            "field": "projectIdentifier",
            "rejectedValue": null,
            "bindingFailure": false,
            "code": "NotBlank"
        },
        {
            "codes": [
                "NotBlank.project.description",
                "NotBlank.description",
                "NotBlank.java.lang.String",
                "NotBlank"
            ],
            "arguments": [
                {
                    "codes": [
                        "project.description",
                        "description"
                    ],
                    "arguments": null,
                    "defaultMessage": "description",
                    "code": "description"
                }
            ],
            "defaultMessage": "Project description is required",
            "objectName": "project",
            "field": "description",
            "rejectedValue": null,
            "bindingFailure": false,
            "code": "NotBlank"
        },
        {
            "codes": [
                "NotBlank.project.projectName",
                "NotBlank.projectName",
                "NotBlank.java.lang.String",
                "NotBlank"
            ],
            "arguments": [
                {
                    "codes": [
                        "project.projectName",
                        "projectName"
                    ],
                    "arguments": null,
                    "defaultMessage": "projectName",
                    "code": "projectName"
                }
            ],
            "defaultMessage": "Project name is required",
            "objectName": "project",
            "field": "projectName",
            "rejectedValue": null,
            "bindingFailure": false,
            "code": "NotBlank"
        }
    ],
    "message": "Validation failed for object='project'. Error count: 3",
```

- Add the BindingResult object as a controller parameter to get access to the errors that occured

### Section 2 Lecture 14

- Fields marked with @Column will have the constraints handled by the database, not the @Valid
- If you added a duplicate with a field marked @Column(unique = true) then you'll need to handle that exception differently

### Section 2 Lecture 16

- Returning an Iterable returns a collection of all the ojects in their JSON representation

### Section 3 Lecture 23

- React components can only have one parent div, nest everything within that component inside

### Transpile with Babel

- npm install babel-cli@6 babel-preset-react-app@3
- npx babel --watch src --out-dir lib --presets react-app/prod

### Section 3 Lecture 27

- The name attributes of inputs that will be used to send json data to a rest service need to match exactly to what the server is expecting in the json payload

````html
<input
  type="text"
  className="form-control form-control-lg"
  placeholder="Unique Project ID"
  disabled
  name="projectIdentifier"
/>
``` * ^ would be sent to a server expect an entity with a String
projectIdentifier property
````

- What a compenent for an entity form would look like:

```javascript
import React, { Component } from "react";

class AddProject extends Component {
  constructor() {
    super();
    this.state = {
      projectName: "",
      projectIdentifier: "",
      description: "",
      startDate: "",
      endDate: ""
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  render() {
    return (
      <div className="project">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h5 className="display-4 text-center">
                Create / Edit Project form
              </h5>
              <hr />
              <form>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg "
                    placeholder="Project Name"
                    name="projectName"
                    value={this.state.projectName}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Unique Project ID"
                    name="projectIdentifier"
                    value={this.state.projectIdentifier}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <textarea
                    className="form-control form-control-lg"
                    placeholder="Project Description"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </div>
                <h6>Start Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="startDate"
                    value={this.state.startDate}
                    onChange={this.onChange}
                  />
                </div>
                <h6>Estimated End Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="endDate"
                    value={this.state.endDate}
                    onChange={this.onChange}
                  />
                </div>

                <input
                  type="submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddProject;
```

### Section 4 Lecture 45
* FetchType.EAGER will have information available as soon as you query the parent entity
* CascadeType.ALL indicates that this entity is the owning side of the relationship. If it is deleted then all associations are also deleted
* Entity relationships will cause infinite recursion when converted to json. @JsonIgnore the child fields to break this
