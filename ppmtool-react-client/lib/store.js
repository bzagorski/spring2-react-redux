import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

var initalState = {};
var middleware = [thunk];

var store = void 0;

if (window.navigator.userAgent.includes("Firefox")) {
  store = createStore(rootReducer, initalState, compose(applyMiddleware.apply(undefined, middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
} else {
  store = createStore(rootReducer, initalState, compose(applyMiddleware.apply(undefined, middleware)));
}

export default store;