var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from "react";
import ProjectItem from "./project/ProjectItem";
import CreateProjectButton from "./project/CreateProjectButton";
import { connect } from "react-redux";
import { GET_PROJECTS } from "../actions/projectActions";
import { getProjects } from "../actions/projectActions";
import PropTypes from "prop-types";
import RefreshTestProjectsButton from "./project/RefreshTestProjectsButton";

var Dashboard = function (_Component) {
  _inherits(Dashboard, _Component);

  function Dashboard() {
    _classCallCheck(this, Dashboard);

    return _possibleConstructorReturn(this, (Dashboard.__proto__ || Object.getPrototypeOf(Dashboard)).apply(this, arguments));
  }

  _createClass(Dashboard, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.getProjects();
    }
  }, {
    key: "render",
    value: function render() {
      var projects = this.props.project.projects;

      return React.createElement(
        "div",
        { className: "projects" },
        React.createElement(
          "div",
          { className: "container" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-md-12" },
              React.createElement(
                "h1",
                { className: "display-4 text-center" },
                "Projects"
              ),
              React.createElement("br", null),
              React.createElement(
                "div",
                { className: "container" },
                React.createElement(CreateProjectButton, null)
              ),
              React.createElement(
                "div",
                { className: "container" },
                React.createElement(RefreshTestProjectsButton, null)
              ),
              React.createElement("br", null),
              React.createElement("hr", null),
              /* The Dashboard can pass properties on to child elements such as the Project Item */
              projects.map(function (project) {
                return React.createElement(ProjectItem, { key: project.id, project: project });
              })
            )
          )
        )
      );
    }
  }]);

  return Dashboard;
}(Component);

Dashboard.propTypes = {
  project: PropTypes.object.isRequired,
  getProjects: PropTypes.func.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    project: state.project
  };
};

export default connect(mapStateToProps, { getProjects: getProjects })(Dashboard);