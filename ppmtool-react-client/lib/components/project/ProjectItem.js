var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { deleteProject } from "../../actions/projectActions";

var ProjectItem = function (_Component) {
  _inherits(ProjectItem, _Component);

  function ProjectItem() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ProjectItem);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ProjectItem.__proto__ || Object.getPrototypeOf(ProjectItem)).call.apply(_ref, [this].concat(args))), _this), _this.onDeleteClick = function (projectIdentifier) {
      _this.props.deleteProject(projectIdentifier);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ProjectItem, [{
    key: "render",
    value: function render() {
      var project = this.props.project;

      return React.createElement(
        "div",
        { className: "container" },
        React.createElement(
          "div",
          { className: "card card-body bg-light mb-3" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-2" },
              React.createElement(
                "span",
                { className: "mx-auto" },
                project.projectIdentifier
              )
            ),
            React.createElement(
              "div",
              { className: "col-lg-6 col-md-4 col-8" },
              React.createElement(
                "h3",
                null,
                project.projectName
              ),
              React.createElement(
                "p",
                null,
                project.description
              )
            ),
            React.createElement(
              "div",
              { className: "col-md-4 d-none d-lg-block" },
              React.createElement(
                "ul",
                { className: "list-group" },
                React.createElement(
                  "a",
                  { href: "#" },
                  React.createElement(
                    "li",
                    { className: "list-group-item board" },
                    React.createElement(
                      "i",
                      { className: "fa fa-flag-checkered pr-1" },
                      " Project Board "
                    )
                  )
                ),
                React.createElement(
                  Link,
                  { to: "/updateProject/" + project.projectIdentifier },
                  React.createElement(
                    "li",
                    { className: "list-group-item update" },
                    React.createElement(
                      "i",
                      { className: "fa fa-edit pr-1" },
                      " Update Project Info"
                    )
                  )
                ),
                React.createElement(
                  "li",
                  {
                    className: "list-group-item delete",
                    onClick: this.onDeleteClick.bind(this, project.projectIdentifier)
                  },
                  React.createElement(
                    "i",
                    { className: "fa fa-minus-circle pr-1" },
                    " Delete Project"
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ProjectItem;
}(Component);

ProjectItem.propTypes = {
  deleteProject: PropTypes.func.isRequired
};

export default connect(null, { deleteProject: deleteProject })(ProjectItem);