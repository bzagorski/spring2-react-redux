var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createProject } from "../../actions/projectActions";
import classnames from "classnames";

var AddProject = function (_Component) {
  _inherits(AddProject, _Component);

  function AddProject() {
    _classCallCheck(this, AddProject);

    var _this = _possibleConstructorReturn(this, (AddProject.__proto__ || Object.getPrototypeOf(AddProject)).call(this));

    _this.state = {
      projectName: "",
      projectIdentifier: "",
      description: "",
      startDate: "",
      endDate: "",
      errors: {}
    };

    // bind the events
    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  _createClass(AddProject, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.errors) {
        this.setState({ errors: nextProps.errors });
      }
    }
  }, {
    key: "onChange",
    value: function onChange(event) {
      this.setState(_defineProperty({}, event.target.name, event.target.value));
    }
  }, {
    key: "onSubmit",
    value: function onSubmit(event) {
      event.preventDefault();
      var newProject = {
        projectName: this.state.projectName,
        projectIdentifier: this.state.projectIdentifier,
        description: this.state.description,
        startDate: this.state.startDate,
        endDate: this.state.endDate
      };

      this.props.createProject(newProject, this.props.history);
    }
  }, {
    key: "render",
    value: function render() {
      // Use destructuring to get the errors from the state
      var errors = this.state.errors;

      return React.createElement(
        "div",
        { className: "project" },
        React.createElement(
          "div",
          { className: "container" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-md-8 m-auto" },
              React.createElement(
                "h5",
                { className: "display-4 text-center" },
                "Create / Edit Project form"
              ),
              React.createElement("hr", null),
              React.createElement(
                "form",
                { onSubmit: this.onSubmit },
                React.createElement(
                  "div",
                  { className: "form-group" },
                  React.createElement("input", {
                    type: "text"
                    // default class names followed by contextual class names
                    , className: classnames("form-control form-control-lg", {
                      "is-invalid": errors.projectName
                    }),
                    placeholder: "Project Name",
                    name: "projectName",
                    value: this.state.projectName,
                    onChange: this.onChange
                  }),
                  errors.projectName && React.createElement(
                    "div",
                    { className: "invalid-feedback" },
                    errors.projectName
                  )
                ),
                React.createElement(
                  "div",
                  { className: "form-group" },
                  React.createElement("input", {
                    type: "text",
                    className: classnames("form-control form-control-lg", {
                      "is-invalid": errors.projectIdentifier
                    }),
                    placeholder: "Unique Project ID",
                    name: "projectIdentifier",
                    value: this.state.projectIdentifier,
                    onChange: this.onChange,
                    pattern: ".{4,5}",
                    title: "Identifier must be between 4 and 5 characters"
                  }),
                  errors.projectName && React.createElement(
                    "div",
                    { className: "invalid-feedback" },
                    errors.projectIdentifier
                  )
                ),
                React.createElement(
                  "div",
                  { className: "form-group" },
                  React.createElement("textarea", {
                    className: classnames("form-control form-control-lg", {
                      "is-invalid": errors.description
                    }),
                    placeholder: "Project Description",
                    name: "description",
                    value: this.state.description,
                    onChange: this.onChange
                  }),
                  errors.projectName && React.createElement(
                    "div",
                    { className: "invalid-feedback" },
                    errors.description
                  )
                ),
                React.createElement(
                  "h6",
                  null,
                  "Start Date"
                ),
                React.createElement(
                  "div",
                  { className: "form-group" },
                  React.createElement("input", {
                    type: "date",
                    className: "form-control form-control-lg",
                    name: "startDate",
                    value: this.state.startDate,
                    onChange: this.onChange
                  })
                ),
                React.createElement(
                  "h6",
                  null,
                  "Estimated End Date"
                ),
                React.createElement(
                  "div",
                  { className: "form-group" },
                  React.createElement("input", {
                    type: "date",
                    className: "form-control form-control-lg",
                    name: "endDate",
                    value: this.state.endDate,
                    onChange: this.onChange
                  })
                ),
                React.createElement("input", {
                  type: "submit",
                  className: "btn btn-primary btn-block mt-4",
                  value: "Submit Project"
                })
              )
            )
          )
        )
      );
    }
  }]);

  return AddProject;
}(Component);

AddProject.propTypes = {
  // Declare prop types. This makes sure that you have the prop that you need and that it is the correct type
  createProject: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    // This comes from redux. state -> errors property
    errors: state.errors
  };
};

export default connect(mapStateToProps, { createProject: createProject })(AddProject);