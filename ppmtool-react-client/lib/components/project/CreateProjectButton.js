import React from "react";
import { Link } from "react-router-dom";

function CreateProjectButton() {
  return React.createElement(
    React.Fragment,
    null,
    React.createElement(
      Link,
      { to: "/addProject", className: "btn btn-lg btn-info" },
      "Create a Project"
    )
  );
}

export default CreateProjectButton;