import axios from "axios";
import { GET_ERRORS, GET_PROJECTS, GET_PROJECT, DELETE_PROJECT } from "./types";

// history will be used to push redirect after update
// async always returns a promise
export const createProject = (project, history) => async dispatch => {
  try {
    await axios.post("/api/project", project);
    // Wipe the errors from the state on a good create / update
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    // send user back to the dashboard
    history.push("/dashboard");
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const getProjects = () => async dispatch => {
  const response = await axios.get("/api/project/all");
  dispatch({
    type: GET_PROJECTS,
    payload: response.data
  });
};

export const getProject = id => async dispatch => {
  try {
    const response = await axios.get(`/api/project/${id}`);
    dispatch({
      type: GET_PROJECT,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const deleteProject = projectIdentifier => async dispatch => {
  if (
    window.confirm(
      "Are you sure you want to delete this project? This will delete all data related to it as well."
    )
  ) {
    await axios.delete(`/api/project/${projectIdentifier}`);
    dispatch({
      type: DELETE_PROJECT,
      payload: projectIdentifier //Use this to remove the deleted project from the list
    });
  }
};

export const refreshProjectList = () => async dispatch => {
  const response = await axios.get("/api/util/refreshList");
  dispatch({
    type: GET_PROJECTS,
    payload: response.data
  });
};
