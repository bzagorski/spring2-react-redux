import axios from "axios";
import { GET_ERRORS, GET_BACKLOG, GET_TASK, DELETE_TASK } from "./types";

export const getTask = (
  projectIdentifier,
  projectSequence
) => async dispatch => {
  try {
    const response = await axios.get(
      `/api/backlog/${projectIdentifier}/${projectSequence}`
    );
    dispatch({
      type: GET_TASK,
      payload: response.data
    });
  } catch (error) {
    console.log(error);

    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const addTask = (projectIdentifier, task, history) => async dispatch => {
  try {
    await axios.post(`/api/backlog/${projectIdentifier}`, task);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    history.push(`/projectBoard/${projectIdentifier}`);
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const updateTask = (
  projectId,
  taskId,
  task,
  history
) => async dispatch => {
  try {
    await axios.patch(`/api/backlog/${projectId}/${taskId}`, task);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    history.push(`/projectBoard/${projectId}`);
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const deleteTask = (projectId, taskId) => async dispatch => {
  if (
    window.confirm(
      `Are you sure you want to delete task: ${taskId}? This action cannot be undone.`
    )
  ) {
    try {
      await axios.delete(`/api/backlog/${projectId}/${taskId}`);
      dispatch({
        type: DELETE_TASK,
        payload: taskId
      });
    } catch (error) {
      dispatch({
        type: GET_ERRORS,
        payload: error.response.data
      });
    }
  }
};

export const getProjectBacklog = projectIdentifier => async dispatch => {
  try {
    const response = await axios.get(`/api/backlog/${projectIdentifier}`);
    dispatch({
      type: GET_BACKLOG,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};
