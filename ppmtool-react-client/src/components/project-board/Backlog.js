import React, { Component } from "react";
import Task from "./tasks/Task";

class Backlog extends Component {
  render() {
    const { tasks } = this.props;
    const propTasks = tasks.map(t => <Task key={t.id} task={t} />);

    let todoTasks = [];
    let inProgressTasks = [];
    let doneTasks = [];

    propTasks.forEach(task => {
      if (task.props.task.status === "TO_DO") {
        todoTasks.push(task);
      } else if (task.props.task.status === "IN_PROGRESS") {
        inProgressTasks.push(task);
      } else if (task.props.task.status === "DONE") {
        doneTasks.push(task);
      }
    });

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="card text-center mb-2">
              <div className="card-header bg-secondary text-white">
                <h3>TO DO</h3>
              </div>
            </div>
            {todoTasks}
          </div>
          <div className="col-md-4">
            <div className="card text-center mb-2">
              <div className="card-header bg-primary text-white">
                <h3>In Progress</h3>
              </div>
            </div>
            {inProgressTasks}
          </div>
          <div className="col-md-4">
            <div className="card text-center mb-2">
              <div className="card-header bg-success text-white">
                <h3>Done</h3>
              </div>
            </div>
            {doneTasks}
          </div>
        </div>
      </div>
    );
  }
}

export default Backlog;
