import React, { Component } from "react";
import { Link } from "react-router-dom";
import { deleteTask } from "../../../actions/backlogActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Task extends Component {
  onDeleteClick = (projectIdentifier, projectSequence) => {
    this.props.deleteTask(projectIdentifier, projectSequence);
  };
  render() {
    const { task } = this.props;
    let priorityClass = getPriorityClass(task);

    return (
      <div className="card mb-1 bg-light">
        <div className={`card-header text-primary ${priorityClass}`}>
          ID: {task.projectSequence} -- Priority: {task.priority}
        </div>
        <div className="card-body bg-light">
          <h5 className="card-title">{task.summary}</h5>
          <p className="card-text text-truncate ">{task.acceptanceCriteria}</p>
          <Link
            to={`/updateTask/${task.projectIdentifier}/${task.projectSequence}`}
            className="btn btn-primary"
          >
            View / Update
          </Link>
          <span
            className="btn btn-danger ml-4"
            onClick={this.onDeleteClick.bind(
              this,
              task.projectIdentifier,
              task.projectSequence
            )}
          >
            Delete
          </span>
        </div>
      </div>
    );
  }
}

const getPriorityClass = task => {
  if (task.priority === "HIGH") {
    return "bg-danger text-light";
  } else if (task.priority === "MEDIUM") {
    return "bg-warning text-light";
  } else if (task.priority === "LOW") {
    return "bg-info text-light";
  }
};

Task.propTypes = {
  deleteTask: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteTask }
)(Task);
