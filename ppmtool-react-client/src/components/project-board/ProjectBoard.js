import React, { Component } from "react";
import { Link } from "react-router-dom";
import Backlog from "./Backlog";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getProjectBacklog } from "../../actions/backlogActions";

class ProjectBoard extends Component {
  constructor() {
    super();
    this.state = {
      errors: {}
    };
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getProjectBacklog(id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
      document.getElementById("backlog").style.display == "none";
    }
  }

  render() {
    const { id } = this.props.match.params;
    const { tasks } = this.props.backlog;
    const { errors } = this.state;
    let BoardContent;

    const loadBoard = errors => {
      if (errors.message) {
        return (
          <div className="alert alert-danger text-center" role="alert">
            {errors.message}
          </div>
        );
      } else {
        return <Backlog tasks={tasks} />;
      }
    };

    BoardContent = loadBoard(errors);

    return (
      <div id="backlog" className="container">
        <Link to={`/addTask/${id}`} className="btn btn-primary mb-3">
          <i className="fas fa-plus-circle"> Create Project Task</i>
        </Link>
        <br />
        <hr />
        {BoardContent}
      </div>
    );
  }
}

ProjectBoard.propTypes = {
  backlog: PropTypes.object.isRequired,
  getProjectBacklog: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  backlog: state.backlog,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getProjectBacklog }
)(ProjectBoard);
