import React, { Component } from "react";
import axios from "axios";

class RefreshTestProjectsButton extends Component {
  constructor() {
    super();

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    axios
      .get("/api/util/refreshList")
      .then(response => window.location.reload());
  }
  render() {
    return (
      <div className="containser">
        <button className="btn btn-large btn-dark" onClick={this.handleClick}>
          Refresh Test Project List
        </button>
      </div>
    );
  }
}

export default RefreshTestProjectsButton;
