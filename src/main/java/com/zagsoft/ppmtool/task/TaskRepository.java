package com.zagsoft.ppmtool.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

    Iterable<Task> findByProjectIdentifierOrderByPriority(String projectIdentifier);

    Optional<Task> findByProjectSequence(String sequence);
}
