package com.zagsoft.ppmtool.task;

import com.zagsoft.ppmtool.backlog.Backlog;
import com.zagsoft.ppmtool.backlog.BacklogRepository;
import com.zagsoft.ppmtool.project.ProjectIdException;
import com.zagsoft.ppmtool.project.ProjectNotFoundException;
import com.zagsoft.ppmtool.project.ProjectRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Slf4j
@Service
public class TaskService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final BacklogRepository backlogRepository;

    @Autowired
    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository,
            BacklogRepository backlogRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.backlogRepository = backlogRepository;
    }

    public Iterable<Task> findBacklogById(String backlogId) {
        return projectRepository.findByProjectIdentifier(backlogId).map(project -> {
            var tasks = taskRepository.findByProjectIdentifierOrderByPriority(backlogId);
            if (tasks.iterator().hasNext()) {
                return tasks;
            } else {
                throw new ProjectNotFoundException("No tasks exist for this project: " + backlogId);
            }
        }).orElseThrow(() -> new ProjectNotFoundException(String.format("Project [%s] does not exist.", backlogId)));
    }

    public Task findTaskByProjectSequence(String projectIdentifier, String projectSequence) {
        return backlogRepository.findByProjectIdentifier(projectIdentifier)
                .map(backlog -> taskRepository.findByProjectSequence(uppercase(projectSequence))
                        .filter(validateTaskIsPartOfProject(projectIdentifier))
                        .orElseThrow(() -> new TaskNotFoundException("No task found with that sequence")))
                .orElseThrow(() -> new ProjectNotFoundException("No project found with that identifier"));
    }

    public Task addTask(String projectIdentifier, Task task) {
        return backlogRepository.findByProjectIdentifier(uppercase(projectIdentifier))
                .map(backlog -> taskRepository.save(initializeTask(projectIdentifier, task, backlog)))
                .orElseThrow(() -> new ProjectIdException("Project not found"));
    }

    public Task updateBySequence(Task updatedTask, String projectIdentifier, String projectSequence) {
        var existingTask = findTaskByProjectSequence(projectIdentifier, projectSequence);

        existingTask.setSummary(updatedTask.getSummary());
        existingTask.setAcceptanceCriteria(updatedTask.getAcceptanceCriteria());
        existingTask.setPriority(updatedTask.getPriority());
        existingTask.setStatus(updatedTask.getStatus());
        existingTask.setDueDate(updatedTask.getDueDate());

        return taskRepository.save(existingTask);
    }

    public void deleteTaskBySequence(String projectIdentifier, String projectSequence) {
        var task = findTaskByProjectSequence(projectIdentifier, projectSequence);
        taskRepository.delete(task);
    }

    private Task initializeTask(String projectIdentifier, Task task, Backlog backlog) {
        task.setBacklog(backlog);
        // todo: this is not thread safe
        var backlogSequence = backlog.getTaskSequence();
        backlogSequence++;
        backlog.setTaskSequence(backlogSequence);
        task.setProjectSequence(projectIdentifier + "-" + backlogSequence);
        task.setProjectIdentifier(projectIdentifier);
        return task;
    }

    private String uppercase(String str) {
        return str != null ? str.toUpperCase() : "";
    }

    private Predicate<Task> validateTaskIsPartOfProject(String projectIdentifier) {
        return task -> {
            var matchesProject = task.getProjectIdentifier().toLowerCase().equals(projectIdentifier.toLowerCase());
            if (matchesProject) {
                return true;
            } else {
                log.info("Task {} is not assigned to project {}.", task.getProjectSequence(), projectIdentifier);
                var errorMessage = String.format("Task [%s] does not exist in project [%s]", task.getProjectSequence(),
                        projectIdentifier);
                throw new TaskNotFoundException(errorMessage);
            }
        };
    }
}
