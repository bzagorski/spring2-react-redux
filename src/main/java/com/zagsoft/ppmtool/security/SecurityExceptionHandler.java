package com.zagsoft.ppmtool.security;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class SecurityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> handleUserNameAlreadyExists(UsernameAlreadyExistsException ex) {
        var response = new UsernameAlreadyExistsResponse(ex.getMessage());
        return ResponseEntity.badRequest().body(response);
    }
}
