package com.zagsoft.ppmtool.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public User saveUser(User newUser) {
        try {
            newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
            newUser.setConfirmPassword(""); // obfuscate from json response
            return userRepository.save(newUser);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new UsernameAlreadyExistsException("Username: " + newUser.getUsername() + " already exists.");
        }

        // todo: username has to be unique
        // todo: password and confirm password must match
        // todo: don't persist or show the confirm password
    }
}
