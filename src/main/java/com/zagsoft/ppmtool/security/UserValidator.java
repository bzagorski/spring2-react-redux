package com.zagsoft.ppmtool.security;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        var user = (User) target;
        if (user.getPassword().length() < 6) {
            // field value needs to match what is defined in the class
            errors.rejectValue("password", "Length", "Password must be at least 6 characters.");
        }
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "Match", "Passwords do not match.");
        }
    }
}
