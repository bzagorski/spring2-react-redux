package com.zagsoft.ppmtool.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class JwtTokenProvider {
    // Generate token
    public String generateToken(Authentication auth) {
        var user = (User) auth.getPrincipal();
        var now = new Date(System.currentTimeMillis());
        var expiryDate = new Date(now.getTime() + SecurityConstants.EXPIRATION);
        var userId = Long.toString(user.getId());
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("id", userId);
        userMap.put("username", user.getUsername());
        userMap.put("fullName", user.getFullName());

        return Jwts.builder()
                .setSubject(userId)
                .setClaims(userMap)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
                .compact();
    }

    // Validate token
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(SecurityConstants.SECRET).parseClaimsJws(token);
            return true;
        } catch (SignatureException se) {
            log.info("Invalid JWT Signature.");
        } catch (MalformedJwtException mje) {
            log.info("Invalid JWT Token.");
        } catch (ExpiredJwtException eje) {
            log.info("Expired JWT Token.");
        } catch (UnsupportedJwtException usje) {
            log.info("Unsupported JWT Token.");
        } catch (IllegalArgumentException iae) {
            log.info("JWT claims string is empty");
        }
        return false;
    }

    // Get user id from the token
    public Long getUserIdFromJWT(String token) {
        var claims = Jwts.parser().setSigningKey(SecurityConstants.SECRET).parseClaimsJws(token).getBody();
        return Long.parseLong((String) claims.get("id"));
    }
}
