package com.zagsoft.ppmtool.security;

import lombok.Data;

@Data
public class UsernameAlreadyExistsResponse {
    private final String username;
}
