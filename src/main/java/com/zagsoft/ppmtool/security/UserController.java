package com.zagsoft.ppmtool.security;

import com.zagsoft.ppmtool.payload.JwtLoginSuccessResponse;
import com.zagsoft.ppmtool.payload.LoginRequest;
import com.zagsoft.ppmtool.util.ValidationErrorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserValidator userValidator;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final ValidationErrorService errorService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult result) {
        return errorService.handleAnyErrors(result)
                .orElseGet(() -> {
                    var auth = authenticationManager.authenticate(
                            new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
                    );
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    var jwt = SecurityConstants.TOKEN_PREFIX + jwtTokenProvider.generateToken(auth);
                    return ResponseEntity.ok(new JwtLoginSuccessResponse(true, jwt));
                });

    }

    @PostMapping("/register")
    public ResponseEntity<?> registerNewUser(@Valid @RequestBody User user, BindingResult result) {
        userValidator.validate(user, result);
        return errorService.handleAnyErrors(result)
                .orElseGet(() -> new ResponseEntity<>(userService.saveUser(user), HttpStatus.CREATED));

    }
}