package com.zagsoft.ppmtool.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Interface that provides the implementation for commence.
 * This is called whenever an exception is thrown when a user
 * attempts to access a restricted resource.
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) throws IOException, ServletException {
        var invalidLoginResponse = new InvalidLoginResponse();
        var json = objectMapper.writeValueAsString(invalidLoginResponse);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(401);
        response.getWriter().print(json);
    }
}
