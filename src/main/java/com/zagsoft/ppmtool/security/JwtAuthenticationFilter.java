package com.zagsoft.ppmtool.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        getTokenFromRequest(request)
                .ifPresent(jwt -> {
                    try {
                        if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                            var userId = tokenProvider.getUserIdFromJWT(jwt);
                            var userDetails = userDetailsService.loadUserById(userId);

                            var usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(
                                    userDetails, null, Collections.emptyList()
                            );

                            usernamePasswordAuthToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthToken);
                            logger.info("Set authentication");
                        }
                    } catch (Exception e) {
                        logger.error("Could not set user authentication in security context", e);
                    }
                });
        filterChain.doFilter(request, response);
        logger.info("Filter chain applied");
    }

    private Optional<String> getTokenFromRequest(HttpServletRequest request) {
        var token = request.getHeader(SecurityConstants.HEADER_STRING);
        if (StringUtils.hasText(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            return Optional.of(token.substring(SecurityConstants.TOKEN_PREFIX.length()));
        }
        return Optional.empty();
    }
}
