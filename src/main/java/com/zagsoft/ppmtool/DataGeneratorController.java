package com.zagsoft.ppmtool;

import com.zagsoft.ppmtool.project.Project;
import com.zagsoft.ppmtool.project.ProjectService;
import com.zagsoft.ppmtool.security.User;
import com.zagsoft.ppmtool.security.UserRepository;
import com.zagsoft.ppmtool.task.Task;
import com.zagsoft.ppmtool.task.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.IntStream;

@RestController
@RequiredArgsConstructor
public class DataGeneratorController {

    private final ProjectService projectService;
    private final TaskService taskService;
    private final UserRepository userRepository;

    @GetMapping("/api/util/refreshList")
    public ResponseEntity<?> refreshTestProjects() {
        projectService.finalAllProjects()
                .forEach(project -> projectService.deleteProjectByIdentifier(project.getProjectIdentifier()));

        IntStream.rangeClosed(1, 5).forEach(i -> {
            var startDate = new Date();
            var endDate = Date.from(LocalDateTime.now().plusDays(i).toInstant(ZoneOffset.UTC));

            var project = Project.builder().projectName("Project " + i)
                    .description("This is the description for Project " + i).projectIdentifier("IDP" + i)
                    .startDate(startDate).endDate(endDate).build();
            var user = User.builder().username("dataProvider@test.com")
                    .password("password")
                    .confirmPassword("password")
                    .fullName("Date Initializer User")
                    .build();
            userRepository.save(user);
            projectService.saveOrUpdate(project, user.getUsername());

            for (int j = 0; j < 10; j++) {
                var random = new SecureRandom();
                var priority = Arrays.asList("LOW", "MEDIUM", "HIGH").get(random.nextInt(3));
                var status = Arrays.asList("TO_DO", "IN_PROGRESS", "DONE").get(random.nextInt(3));
                var task = Task.builder().summary(project.getProjectName() + " - Task " + (j + 1))
                        .acceptanceCriteria("Some demo acceptance criteria.").dueDate(new Date())
                        .priority(Task.Priority.valueOf(priority)).status(Task.Status.valueOf(status)).build();
                taskService.addTask(project.getProjectIdentifier(), task);
            }
        });
        return ResponseEntity.ok("Test Projects refreshed");
    }
}