package com.zagsoft.ppmtool.backlog;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.zagsoft.ppmtool.project.Project;
import com.zagsoft.ppmtool.task.Task;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Backlog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Builder.Default
    private Integer taskSequence = 0;

    private String projectIdentifier;

    @JsonBackReference // Ignore on the child side to break infinite recursion
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Builder.Default
    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, mappedBy = "backlog", orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();
}