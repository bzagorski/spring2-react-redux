package com.zagsoft.ppmtool.backlog;

import com.zagsoft.ppmtool.task.Task;
import com.zagsoft.ppmtool.task.TaskService;
import com.zagsoft.ppmtool.util.ValidationErrorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/backlog")
public class BacklogController {

    private final TaskService taskService;
    private final ValidationErrorService errorService;

    public BacklogController(TaskService taskService, ValidationErrorService errorService) {
        this.taskService = taskService;
        this.errorService = errorService;
    }

    @GetMapping("/{projectIdentifier}")
    public ResponseEntity<Iterable<Task>> getProjectBacklog(@PathVariable String projectIdentifier) {
        return new ResponseEntity<>(taskService.findBacklogById(projectIdentifier), HttpStatus.OK);
    }

    @GetMapping("/{projectIdentifier}/{projectSequence}")
    public ResponseEntity<?> getProjectTask(@PathVariable String projectIdentifier,
            @PathVariable String projectSequence) {
        var task = taskService.findTaskByProjectSequence(projectIdentifier, projectSequence);
        return ResponseEntity.ok().body(task);
    }

    @PostMapping("/{projectIdentifier}")
    public ResponseEntity<?> addTaskToBacklog(@Valid @RequestBody Task task, BindingResult result,
            @PathVariable String projectIdentifier) {

        return errorService.handleAnyErrors(result).orElseGet(() -> {
            var newTask = taskService.addTask(projectIdentifier, task);
            return new ResponseEntity<>(newTask, HttpStatus.CREATED);
        });
    }

    @PatchMapping("/{projectIdentifier}/{projectSequence}")
    public ResponseEntity<?> updateTask(@Valid @RequestBody Task task, BindingResult result,
            @PathVariable String projectIdentifier, @PathVariable String projectSequence) {
        return errorService.handleAnyErrors(result).orElseGet(() -> new ResponseEntity<>(
                taskService.updateBySequence(task, projectIdentifier, projectSequence), HttpStatus.OK));
    }

    @DeleteMapping("/{projectIdentifier}/{projectSequence}")
    public ResponseEntity<?> deleteTask(@PathVariable String projectIdentifier, @PathVariable String projectSequence) {
        taskService.deleteTaskBySequence(projectIdentifier, projectSequence);
        return ResponseEntity.ok("Task " + projectSequence + " was deleted.");
    }
}
