package com.zagsoft.ppmtool.project;

import com.zagsoft.ppmtool.backlog.Backlog;
import com.zagsoft.ppmtool.backlog.BacklogRepository;
import com.zagsoft.ppmtool.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final BacklogRepository backlogRepository;
    private final UserRepository userRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, BacklogRepository backlogRepository, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.backlogRepository = backlogRepository;
        this.userRepository = userRepository;
    }

    public Project saveOrUpdate(Project project, String username) {
        try {
            var user = userRepository.findByUsername(username)
                    .orElseThrow(() -> new IllegalArgumentException("User " + username + " not found"));
            project.setUser(user);
            project.setProjectLeader(user.getUsername());
            project.setProjectIdentifier(upperCaseString(project.getProjectIdentifier()));
            if (project.getId() == null) {
                var backlog = Backlog.builder()
                        .project(project)
                        .projectIdentifier(upperCaseString(project.getProjectIdentifier()))
                        .build();
                project.setBacklog(backlog);

                if (project.getId() != null) {
                    backlogRepository.findByProjectIdentifier(project.getProjectIdentifier())
                            .ifPresent(project::setBacklog);
                }
            }
            return projectRepository.save(project);
        } catch (Exception e) {
            throw new ProjectIdException(String.format("Project identifier [%s] already exists", project.getProjectIdentifier()));
        }
    }

    public Project findProjectByIdentifier(String projectIdentifier) {
        return projectRepository.findByProjectIdentifier(upperCaseString(projectIdentifier))
                .orElseThrow(() -> new ProjectIdException(String.format("No project found with id [%s]", projectIdentifier)));
    }

    public Iterable<Project> finalAllProjects() {
        return projectRepository.findAll();
    }

    public void deleteProjectByIdentifier(String identifier) {
        projectRepository.findByProjectIdentifier(upperCaseString(identifier))
                .ifPresentOrElse(
                        projectRepository::delete,
                        () -> {
                            throw new ProjectIdException(String.format("No project found with identifier [%s]", identifier));
                        });
    }

    private String upperCaseString(String string) {
        return string != null ? string.toUpperCase() : "";
    }
}
