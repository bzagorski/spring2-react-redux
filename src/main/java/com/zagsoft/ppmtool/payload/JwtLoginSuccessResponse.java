package com.zagsoft.ppmtool.payload;

import lombok.Data;

@Data
public class JwtLoginSuccessResponse {
    private final boolean success;
    private final String token;
}
