package com.zagsoft.ppmtool.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ValidationErrorService {

    public Optional<ResponseEntity<?>> handleAnyErrors(BindingResult result) {
        if (result.hasErrors()) {
            var errorMap = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(
                            FieldError::getField,
                            fieldError -> fieldError.getDefaultMessage() != null ? fieldError.getDefaultMessage() : "",
                            ValidationErrorService::mergeMessages));
            return Optional.of(new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST));
        } else {
            return Optional.empty();
        }
    }

    private static String mergeMessages(String a, String b) {
        return a + ". " + b;
    }

}
