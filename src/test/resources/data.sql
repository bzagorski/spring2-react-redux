insert into PROJECT (id, project_name, project_identifier, description) values (1, 'Project 1', 'idt1', 'This is test project 1')
insert into PROJECT (id, project_name, project_identifier, description) values (2, 'Project 2', 'idt2', 'This is test project 2')
insert into PROJECT (id, project_name, project_identifier, description) values (3, 'Project 3', 'idt3', 'This is test project 3')

insert into BACKLOG (id, project_identifier, project_id) values (1, 'idt1', 1)
insert into BACKLOG (id, project_identifier, project_id) values (2, 'idt2', 2)
insert into BACKLOG (id, project_identifier, project_id) values (3, 'idt3', 3)

insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (1, 'idt1', 'project 1 task 1', 1, 0, 'idt1-1')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (2, 'idt1', 'project 1 task 2', 1, 0, 'idt1-2')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (3, 'idt1', 'project 1 task 3', 1, 1, 'idt1-3')

insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (4, 'idt2', 'project 2 task 1', 2, 0, 'idt2-1')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (5, 'idt2', 'project 2 task 2', 2, 2, 'idt2-2')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (6, 'idt2', 'project 2 task 3', 2, 1, 'idt2-3')

insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (7, 'idt3', 'project 3 task 1', 3, 0, 'idt3-1')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (8, 'idt3', 'project 3 task 2', 3, 0, 'idt3-2')
insert into TASK (id, project_identifier, summary, backlog_id, priority, project_sequence) values (9, 'idt3', 'project 3 task 3', 3, 0, 'idt3-3')