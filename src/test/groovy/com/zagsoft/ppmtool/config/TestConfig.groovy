package com.zagsoft.ppmtool.config

import com.zagsoft.ppmtool.project.ProjectService
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import spock.mock.DetachedMockFactory

@TestConfiguration
class TestConfig {

    def mockFactory = new DetachedMockFactory()

    @Bean
    ProjectService projectServiceMock() {
        return mockFactory.Mock(ProjectService)
    }
}
