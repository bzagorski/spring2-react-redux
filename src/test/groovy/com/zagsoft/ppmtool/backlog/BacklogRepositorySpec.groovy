package com.zagsoft.ppmtool.backlog

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.ComponentScan
import spock.lang.Specification

@DataJpaTest
@ComponentScan("com.zagsoft.ppmtool")
class BacklogRepositorySpec extends Specification {

    @Autowired
    BacklogRepository backlogRepository

    def "Find backlog by project identifier"() {
        expect:
        backlogRepository.findByProjectIdentifier('idt1')
                .isPresent()
    }
}
