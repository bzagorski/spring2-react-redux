package com.zagsoft.ppmtool.backlog

import com.zagsoft.ppmtool.task.Task
import com.zagsoft.ppmtool.task.TaskService
import com.zagsoft.ppmtool.util.ValidationErrorService
import groovy.json.JsonOutput
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.hamcrest.Matchers.containsString
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class BacklogControllerSpec extends Specification {

    def taskService = Mock(TaskService)
    def validationService = Mock(ValidationErrorService)
    def backlogController = new BacklogController(taskService, validationService)
    def mockMvc = MockMvcBuilders.standaloneSetup(backlogController).build()
    def tasks = [Task.builder()
                         .projectIdentifier('project1')
                         .projectSequence('project1-1')
                         .summary('task 1').build(),
                 Task.builder()
                         .projectIdentifier('project2')
                         .projectSequence('project2-1')
                         .summary('task 2').build()]

    def "Get project backlog"() {
        given:
        taskService.findBacklogById('project-1') >> tasks

        expect:
        mockMvc.perform(get('/api/backlog/project-1'))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString('task 1')))

    }

    def "Get project task by project and sequence"() {
        given:
        taskService.findTaskByProjectSequence('project1', 'project-1') >> tasks[0]

        expect:
        mockMvc.perform(get('/api/backlog/project1/project-1'))
            .andExpect(status().isFound())
            .andExpect(content().string(containsString('project1-1')))
    }

    def "Add task to backlog"() {
        given:
        validationService.handleAnyErrors(_) >> Optional.empty()
        taskService.addTask('project1', _) >> tasks[0]

        expect:
        mockMvc.perform(post('/api/backlog/project1')
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(tasks[0]).toString())
        ).andExpect(status().isCreated())
    }
}
