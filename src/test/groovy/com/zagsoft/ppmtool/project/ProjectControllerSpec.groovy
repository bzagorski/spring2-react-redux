package com.zagsoft.ppmtool.project


import com.zagsoft.ppmtool.util.ValidationErrorService
import groovy.json.JsonOutput
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ProjectControllerSpec extends Specification {

    def projectService = Mock(ProjectService)
    def validationService = Mock(ValidationErrorService)
    def projectIdExceptionHandler = Mock(ProjectExceptionHandler)
    def projectController = new ProjectController(projectService, validationService)
    def mockMvc = MockMvcBuilders.standaloneSetup(projectController)
            .setControllerAdvice(projectIdExceptionHandler)
            .build()

    def "Save valid project"() {
        given:
        def project = Project.builder()
                .description('This is the description')
                .projectIdentifier('ABCD')
                .projectName('Test Project').build()

        validationService.handleAnyErrors(_) >> Optional.empty()
        projectService.saveOrUpdate(project) >> project

        expect:
        mockMvc.perform(post('/api/project')
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(project).toString())
        ).andExpect(status().isCreated())
    }

    def "Save invalid project"() {
        given:
        def invalid = getInvalidProject()
        validationService.handleAnyErrors(_) >> Optional.of(new ResponseEntity(["errorMessage": "Project Name is required"], HttpStatus.BAD_REQUEST))

        expect:
        mockMvc.perform(post('/api/project')
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(invalid).toString()))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"errorMessage\":\"Project Name is required\"}"))
    }

    def "Find project by project identifier"() {
        given:
        def project = getValidProject()
        projectService.findProjectByIdentifier("TSPR") >> project

        expect:
        def result = mockMvc.perform(get('/api/project/TSPR'))
                .andExpect(status().isOk())
                .andReturn()
        result.response.contentAsString.contains('"projectName":"Test Project"')
    }

    def "Unable to find project with given identifier"() {
        given:
        projectService.findProjectByIdentifier(_ as String) >> { String identifier ->
            throw new ProjectIdException("No project found with id [$identifier]")}

        when:
        def result = mockMvc.perform(get('/api/project/invl'))

        then:
        result.andExpect(status().isBadRequest())
        result.andReturn().response.contentAsString == '{"success":false,"message":"No project found with id [invl]"}'
    }

    private static def getValidProject() {
        return Project.builder()
                .projectName("Test Project")
                .description("Test Project Description")
                .projectIdentifier("tspr")
                .build()
    }

    private static def getInvalidProject() {
        return new Project()
    }
}
