package com.zagsoft.ppmtool.project


import com.zagsoft.ppmtool.backlog.BacklogRepository
import spock.lang.Specification
import spock.lang.Unroll

class ProjectServiceSpec extends Specification {

    private def projectRepository = Mock(ProjectRepository)
    private def backlogRepository = Mock(BacklogRepository)
    private def projectService = new ProjectService(projectRepository, backlogRepository, userRepository)

    def project

    def setup() {
        project = new Project().builder()
                .projectName("Test Project")
                .description("This for testing purposes only")
                .projectIdentifier("TEst")
                .build()
    }

    def "Save should save"() {
        given:
        projectRepository.save(_) >> project

        expect:
        projectService.saveOrUpdate(project) == project
        project.getProjectIdentifier() == 'TEST'
    }

    def "Exception is thrown when project identifier exists"() {
        given:
        projectRepository.save(project) >>
                { Project p -> throw new ProjectIdException("Project Identifier [$p.projectIdentifier}] already exists") }

        when:
        projectService.saveOrUpdate(project)

        then:
        thrown(ProjectIdException)
    }

    def "Find project by identifier"(String identifier) {
        given:
        projectRepository.findByProjectIdentifier("TEST") >> Optional.of(project)

        expect:
        projectService.findProjectByIdentifier(identifier) == project

        where:
        identifier << ["test", "Test"]
    }

    @Unroll
    def "Project not found by identifier"(String identifier) {
        given:
        projectRepository.findByProjectIdentifier(_) >> Optional.empty()

        when:
        projectService.findProjectByIdentifier(identifier)

        then:
        thrown(ProjectIdException)

        where:
        identifier << ["stet", null]
    }

    def "Find all projects"() {
        given:
        def project2 = Project.builder()
                .projectName("Project 2")
                .description("Project to add to a list")
                .projectIdentifier("tes2")
                .build()

        projectRepository.findAll() >> [project, project2]

        expect:
        projectService.finalAllProjects().size() == 2
    }

    def "Delete found project"() {
        given:
        def p = Optional.of(project)
        projectRepository.findByProjectIdentifier(_) >> p

        when:
        projectService.deleteProjectByIdentifier('test')

        then:
        1 * projectRepository.delete(p.get())
    }

    def "Exception thrown when attempting to delete project that does not exist"() {
        given:
        projectRepository.findByProjectIdentifier(_) >> Optional.empty()

        when:
        projectService.deleteProjectByIdentifier("TEST")

        then:
        thrown(ProjectIdException)
    }
}
