package com.zagsoft.ppmtool.project

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.ComponentScan
import spock.lang.Specification

@DataJpaTest
@ComponentScan("com.zagsoft.ppmtool")
class ProjectRepositorySpec extends Specification {

    @Autowired
    ProjectRepository projectRepository

    def "Should find a project"() {
        expect:
        projectRepository.findAll().size() == 3
    }

    def "Find by project identifier"() {
        when:
        def result = projectRepository.findByProjectIdentifier('idt1')

        then:
        result.isPresent()
        result.get().projectName == 'Project 1'
    }

    def "Attempt to find project that is not present"() {
        when:
        def result = projectRepository.findByProjectIdentifier("blah")

        then:
        result.isEmpty()
    }
}
