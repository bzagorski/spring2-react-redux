package com.zagsoft.ppmtool.task

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.ComponentScan
import spock.lang.Specification

@DataJpaTest
@ComponentScan("com.zagsoft.ppmtool")
class TaskRepositorySpec extends Specification {

    @Autowired
    TaskRepository taskRepository

    def "Find all tasks"() {
        given:
        def result = taskRepository.findAll()
        result.each { println it}

        expect:
        result.size() == 9
    }

    def "Order by Priority"() {
        given:
        def result = taskRepository.findByProjectIdentifierOrderByPriority('idt2')
        result.each { println it}


        expect:
        result[0].summary == 'project 2 task 1'
        result.last().summary == 'project 2 task 2'
    }

    def "Find by Project Sequence"() {
        given:
        def result = taskRepository.findByProjectSequence('idt3-1')

        expect:
        result.get().summary == 'project 3 task 1'
    }

    def "Task should be deleted"() {
        given:
        taskRepository.findByProjectSequence('idt3-1')
                .ifPresent({ task -> taskRepository.delete(task) })

        expect:
        taskRepository.findAll().size() == 8
    }
}
