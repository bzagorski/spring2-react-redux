package com.zagsoft.ppmtool.task

import com.zagsoft.ppmtool.backlog.Backlog
import com.zagsoft.ppmtool.backlog.BacklogRepository
import com.zagsoft.ppmtool.project.ProjectNotFoundException
import com.zagsoft.ppmtool.project.ProjectRepository
import spock.lang.Specification

class TaskServiceSpec extends Specification {

    def taskRepository = Mock(TaskRepository)
    def backlogRepository = Mock(BacklogRepository)
    def projectRepository = Mock(ProjectRepository)
    def taskService = new TaskService(projectRepository, taskRepository, backlogRepository)
    def backlog = Optional.of(Backlog.builder().projectIdentifier('TEST').build())
    def testTask = Task.builder().summary('Test Task')
            .backlog(backlog.get())
            .projectIdentifier('TEST')
            .projectSequence('TEST-1')
            .status(Task.Status.TO_DO)
            .priority(Task.Priority.LOW)
            .build()


    def "Add a task to a project"() {
        given: 'A backlog'
        def task = Task.builder().summary("Test Task").build()
        backlogRepository.findByProjectIdentifier('TEST') >> backlog
        taskRepository.save(task) >> task

        when:
        def result = taskService.addTask('TEST', task)

        then:
        result == testTask
    }

    def "Find tasks by backlog id"() {
        given:
        def tasks = [Task.builder().projectIdentifier('TEST1'),
                     Task.builder().projectIdentifier('TEST1'),
                     Task.builder().projectIdentifier('TEST1')]
        taskRepository.findByProjectIdentifierOrderByPriority('TEST1') >> tasks

        when:
        def result = taskService.findBacklogById('TEST1')

        then:
        result.size() == 3
    }

    def "Throws project not found when project is not found"() {
        given:
        taskRepository.findByProjectIdentifierOrderByPriority(_) >> []

        when:
        taskService.findBacklogById('test')

        then:
        thrown(ProjectNotFoundException)
    }

    def "Find task by project identifier"() {
        given:
        backlogRepository.findByProjectIdentifier("TEST") >> backlog
        taskRepository.findByProjectSequence('TEST-1') >> Optional.of(testTask)

        when:
        def result = taskService.findTaskByProjectSequence('TEST', 'TEST-1')

        then:
        result == testTask
    }

    def "Task exists but is not contained in current project"() {
        def taskInDifferentProject = Task.builder().summary('Test Task')
                .backlog(backlog.get())
                .projectIdentifier('BLAH')
                .projectSequence('TEST-1')
                .status(Task.Status.TO_DO)
                .priority(Task.Priority.LOW)
                .build()
        given:
        backlogRepository.findByProjectIdentifier("TEST") >> backlog
        taskRepository.findByProjectSequence('TEST-1') >> Optional.of(taskInDifferentProject)

        when:
        def result = taskService.findTaskByProjectSequence('TEST', 'TEST-1')

        then:
        thrown(TaskNotFoundException)
    }

    def "Project not found while searching for task"() {
        given:
        backlogRepository.findByProjectIdentifier("TEST") >> Optional.empty()
        taskRepository.findByProjectSequence('TEST-1') >> Optional.of(testTask)

        when:
        def result = taskService.findTaskByProjectSequence('TEST', 'TEST-1')

        then:
        thrown(ProjectNotFoundException)
    }

    def "Update a task"() {
        given:
        backlogRepository.findByProjectIdentifier('TEST') >> backlog
        def updatedTask = testTask
        updatedTask.summary = 'Updated Task Summary'
        taskRepository.findByProjectSequence('TEST-1') >> Optional.of(testTask)
        taskRepository.save(updatedTask) >> updatedTask

        when:
        def result = taskService.updateBySequence(testTask, 'TEST', 'TEST-1')

        then:
        result.summary == 'Updated Task Summary'
    }
}
